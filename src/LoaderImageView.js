import React, {useState} from 'react';
import {StyleSheet, View, Image, ActivityIndicator} from 'react-native';

const LoaderImageView = (props) => {
  const [loading, setLoading] = useState(true);
  const {url, imageWidth = '100%', imageHeight = '100%'} = props;

  return (
    <View style={styles.container}>
      <Image
        style={{width: imageWidth, height: imageHeight}}
        onLoadStart={() => setLoading(true)}
        onLoadEnd={() => setLoading(false)}
        source={{uri: url}}
        resizeMode="stretch"
      />

      <ActivityIndicator
        style={styles.activityIndicator}
        animating={loading}
        size="large"
        color="black"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  imageView: {
    width: '100%',
    height: '100%',
  },
  activityIndicator: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default LoaderImageView;
